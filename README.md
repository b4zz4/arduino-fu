![glenda arduino](logo/logo.png) Arduino `con filosofía Unix`
============================

- Sin usar el IDE de arduino.
- Sin usar java.
- Portable a otros microprocesadores.

## Instalación

`sudo ./instalar.sh`

## compilar y cargar al arduino

<pre>
cd share/arduino/examples/01.Basics/Blink/
arduino-fu # compila
arduino-fu upload # carga en el arduino
</pre>

_**Nota:** dependiendo del modelo de arduino puede ser que tengas que conectar un led, para que el `blink` funcione_

## re-compilar

Si ya compilaste el archivo podrias querer volver a compilarlo, para que se hagan efectivos los cambios

<pre>
cd share/arduino/examples/01.Basics/Blink/
arduino-fu limpiar
arduino-fu upload
</pre>

### Serial

<pre>
cd share/examples/01.Basics/AnalogReadSerial
arduino-fu upload
arduino-fu serial 9600
</pre>

o en lugar de este ultimo comando:

<pre>
stty -F /dev/ttyUSB0 raw speed 9600
</pre>


#### Recibir mensajes

<pre>cat /dev/ttyUSB0</pre>

#### Enviar mensajes

<pre>echo "hello world" > /dev/ttyUSB0</pre>

## Modelos de arduino

Por defecto esta pensado para el `atmega328`. Para cambiarlo por defecto,  hay que cambiar la configuración en `share/arduino/SConstruct`, leer la documentacion en el propio archivo. 
Tambien se puede configurar en `arscons.json` hay uno de ejemplo en `share/arduino/arscons.json` este debe incluirse en cada proyecto.

### Ejemplo

Si tenemos un modelo `arduino uno` solo tenemos que hacer lo siguiente

<pre>
cd share/arduino/examples/01.Basics/Blink/
cp /usr/share/arduino/arscons.json .
arduino-fu upload
</pre>

## Sintaxis

Actualmente están incluidas las sintaxis para emacs, gedit, vim y otros. [Leer documentacion](sintaxis/README.md)

# TVout 

Una salida de audio y video para arduino muy simple de armar. [Leer documentacion](hackvision/README.md)

# Hackvision

Armar un video juego 8bits con tu arduino. [Leer documentacion](hackvision/README.md)

## Nota

- Solo probado en GNU/Linux y atmega328

## Fuente Bibliográfica

- http://syvic.synusia.es/node/7
- http://blog.nemik.net/2011/12/arduino-openwrt-art/
- https://raw.github.com/mignev/arscons/haos/examples/arscons.json

