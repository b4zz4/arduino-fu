#!/usr/bin/env sh

apt-get install -y scons python-serial
apt-get install -y arduino-core
[ ! -d /usr/share/arduino ] && cp -r share/arduino /usr/share/
cp bin/arduino-fu /usr/local/bin/
