Sintaxis
========

gedit y similares
-----------------

~~~
cd sintaxis/GtkSourceView-Arduino/
sudo cp *lang /usr/share/gtksourceview-2.0/language-specs/
~~~

vim
---

~~~
mkdir -p ~/.vim/syntax/
cp sintaxis/vim-arduino/syntax/*.vim ~/.vim/syntax/
cat sintaxis/vim-arduino/ftdetect/*.vi >> ~/.vimrc
~~~

emacs
-----

?

