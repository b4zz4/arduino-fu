#!/bin/sh

if [ ! -d "/usr/share/arduino/" ]; then
	echo "tenes que instalara arduino antes"
	exit
fi

aptitude install imagemagick

cp -r bin/* /usr/bin
cp -r examples /usr/share/arduino
cp -r libraries /usr/share/arduino
